# Containerizing Rust Actix Web Service

## Introduction

This document outlines the process of containerizing a simple Rust Actix web application. The application is a basic web service that calculates change in coins for a given amount in dollars and cents. The focus here is on the containerization using Docker, ensuring the functionality within a container, and documenting the entire process.

## Requirements

- Containerize the Rust Actix web app
- Build a Docker image
- Run the container locally

## Process

### Step 1: Writing the Dockerfile

The `Dockerfile` is written to create a lightweight and secure container for the web service. It is composed of two stages: a build stage and a production stage.

- **Build Stage**: Uses the `rust:1.68` image to build the application.
- **Production Stage**: Uses `gcr.io/distroless/cc-debian11` for a minimal runtime environment.

The application is built in the build stage and then copied to the production stage, ensuring a smaller and more secure final image.

### Step 2: Building the Docker Image

To build the Docker image, the following command is used:

```bash
docker build -t rust-actix-web-service .
```

This command builds the image using the `Dockerfile` in the current directory and tags the image as `rust-actix-web-service`.

### Step 3: Running the Container Locally

Once the image is built, the container can be run using:

```bash
docker run -p 8080:8080 rust-actix-web-service
```

This command runs the container and maps the container's port 8080 to the local port 8080, allowing the web service to be accessed locally.

### Step 4: Verification and Screenshots

After running the container, verification is done by accessing the application's endpoints, ensuring that the application is fully functional within the container. Screenshots or a demo video are taken to demonstrate the running container and the accessible web service.

## Conclusion

The process outlined above describes the containerization of a Rust Actix web service, focusing on creating an efficient Docker image, ensuring the functionality of the containerized application, and providing clear documentation of the process. This meets the specified requirements and addresses the grading criteria effectively.
